package simulator

import org.la4j.matrix.Matrix
import org.la4j.matrix.dense.Basic2DMatrix
import org.la4j.vector.Vector
import org.la4j.vector.dense.BasicVector
import java.awt.Color
import java.awt.Graphics
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import javax.imageio.ImageIO

val DEBUG = false

class Marker(
        var x : Double,
        var y : Double,
        var colorr : Double,
        var colorg : Double,
        var colorb : Double
) {
}

class Environment(
        width_ : Int,
        height_ : Int,
        cellSize : Int,
        val fdt : Double,
        val viscosity : Double,
        val density : Double,
        val markerFrequency : Int,
        val sourceV : Double
) {
    val cellSizeDbg = cellSize

    var step = 0

    val width = width_ / cellSize + 2
    val height = height_ / cellSize + 1

    var markers = ArrayList<Marker>()
    var dt = 0.0

    // u[i][j] = u_{i - 1/2}{j}
    // v[i][j] = v_{i}{j - 1/2}
    // pressure[i][j] = p_{i}{j}
    val u = Array(width, { Array(height, {0.0})})
    val v = Array(width, { Array(height, {0.0})})

    // temporary arrays for evaluating convection
    val tu = Array(width, { Array(height, {0.0})})
    val tv = Array(width, { Array(height, {0.0})})

    var pressure = Array(width, { Array(height, {0.0})})

    var id = Array(width, { Array(height, {-1})})

    var fcount = 0
    var solverT : BlockThomasAlgorithm<Matrix>
    init {
        for (i in 0..width-1) {
            for (j in 0..height-1) {
                if (hasPressure(i, j)) {
                    id[i][j] = fcount
                    fcount++
                }
            }
        }
        val As = ArrayList<Matrix>()
        val Bs = ArrayList<Matrix>()
        val Cs = ArrayList<Matrix>()
        for (i in 1 .. width - 2) {
            val M = Basic2DMatrix(height - 1, height - 1)
            for (j in 0 .. height - 2) {
                M.set(j, j, 4.0)
                if (i == 1 || i == width - 2) {
                    M.set(j, j, M.get(j, j) - 1.0)
                }
                if (j == height - 2) {
                    M.set(j, j, M.get(j, j) - 1.0)
                }
            }
            for (j in 0 .. height - 3) {
                M.set(j, j + 1, -1.0)
                M.set(j + 1, j, -1.0)
            }
            Bs.add(M)
        }
        val I = Basic2DMatrix(height - 1, height - 1)
        for (j in 0 .. height - 2) {
            I.set(j, j, -1.0)
        }
        for (i in 1 .. width - 2) {
            As.add(I.copy()!!)
            Cs.add(I.copy()!!)
        }

        solverT = BlockThomasAlgorithm(width - 2, MatrixField(height - 1), As, Bs, Cs)

        update()
    }

    fun solvePressure(rhs : Vector) : Vector {
        val Ds = ArrayList<Matrix>()
        for (i in 1 .. width - 2) {
            val D = Basic2DMatrix(height - 1, height - 1)
            for (j in 0 .. height - 2) {
                D.set(j, j, rhs.get((i - 1) * (height - 1) + j))
            }
            Ds.add(D)
        }
        val Ps = solverT.compute(Ds)
        val P = BasicVector((width - 2) * (height - 1))
        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                P.set((i - 1) * (height - 1) + j, Ps.get(i - 1).getRow(j)!!.sum())
            }
        }
        return P
    }

    fun update() {
        val it = markers.iterator()
        while (it.hasNext()) {
            val m = it.next()
            if (!(m.y > 0)) {
                it.remove()
            }
        }

        var rr = Math.random()
        var gg = Math.random()
        var bb = Math.random()

        if (step % markerFrequency == 0) {
            for (i in 1 .. width - 2) {
                val q = 4
                for (w in 0..q-1) {
                    markers.add(Marker(i - 0.1 * w, height - 1.1, rr, gg, bb))
                }
                for (w in 0..q-1) {
                    markers.add(Marker(i + 0.1 * w, height - 1.1, rr, gg, bb))
                }
            }
        }

        for (i in 0 .. width - 1) {
            v[i][height - 1] = sourceV
        }

    }

    fun updatedt() : Double {
        var maxu = 0.0
        var maxv = 0.0
        for (i in 0..width-1) {
            for (j in 0..height-1) {
                maxu = Math.max(maxu, Math.abs(u[i][j]))
                maxv = Math.max(maxv, Math.abs(v[i][j]))
            }
        }
        return 0.1 / (maxu + maxv)
    }

    fun interpolateVelocity(x : Double, y : Double) : Pair<Double, Double> {
        // we need to interpolate separately, x with u values, y with v values
        fun bilinear(i : Int, j : Int, dx : Double, dy : Double, a : Array<Array<Double>>) : Double {
            assert(dx <= 1.0 && dy <= 1.0)
            var aa = if (j >= 0) a[i][j] else a[i][0]
            var bb = if (j >= 0) a[i + 1][j] else a[i + 1][0]
            var cc = a[i][j + 1]
            var dd = a[i + 1][j + 1]

            var res = 0.0
            res += (1 - dx) * (1 - dy) * aa
            res += dx * (1 - dy) * bb
            res += (1 - dx) * dy * cc
            res += dx * dy * dd
            return res
        }

        val iu = (x + 0.5).toInt()
        val ju = y.toInt()
        val dxu = x - (iu - 0.5)
        val dyu = y - ju
        val vu = bilinear(iu, ju, dxu, dyu, u)

        val iv = x.toInt()
        val jv = (y - 0.5).toInt()
        val dxv = x - iv
        val dyv = y - (jv - 0.5)
        val vv = bilinear(iv, jv, dxv, dyv, v)

        return Pair(vu, vv)
    }

    fun applyConvection() {
        fun backtraceVelocityRK2(x : Double, y : Double, uu : Double, vv : Double) : Pair<Double, Double> {
            val (vvx, vvy) = interpolateVelocity(x - uu * dt / 2, y - vv * dt / 2)
            return interpolateVelocity(x - vvx * dt, y - vvy * dt)

        }

        fun backtraceVelocityEuler(x : Double, y : Double, uu : Double, vv : Double) : Pair<Double, Double> {
            return interpolateVelocity(x - uu * dt, y - vv * dt)
        }

        // updating u
        for (i in 2 .. width - 2) {
            for (j in 0 .. height - 2) {
                val ux1 = i - 0.5
                val uy1 = j.toDouble()
                var uu1 = interpolateVelocity(ux1, uy1)
                uu1 = backtraceVelocityRK2(ux1, uy1, uu1.first, uu1.second)
                tu[i][j] = uu1.first
            }
        }

        // updating v
        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                val vx1 = i.toDouble()
                val vy1 = j - 0.5
                var vv1 = interpolateVelocity(vx1, vy1)
                vv1 = backtraceVelocityRK2(vx1, vy1, vv1.first, vv1.second)
                tv[i][j] = vv1.second
            }
        }


        for (i in 2 .. width - 2) {
            for (j in 0 .. height - 2) {
                u[i][j] = tu[i][j]
            }
        }

        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                v[i][j] = tv[i][j]
            }
        }
    }

    fun applyViscosity() {
        fun laplacianu(i : Int, j : Int) : Double {
            var res = 0.0
            if (i > 1) {
                res += u[i - 1][j] - u[i][j]
            }
            if (i < width - 1) {
                res += u[i + 1][j] - u[i][j]
            }
            if (j > 0) {
                res += u[i][j - 1] - u[i][j]
            }
            res += u[i][j + 1] - u[i][j]
            return res
        }
        fun laplacianv(i : Int, j : Int) : Double {
            var res = 0.0
//            if (i > 1) {
                res += v[i - 1][j] - v[i][j]
//            }
//            if (i < width - 2) {
                res += v[i + 1][j] - v[i][j]
//            }
            if (j > 0) {
                res += v[i][j - 1] - v[i][j]
            }
            res += v[i][j + 1] - v[i][j]
            return res
        }
        for (i in 1 .. width - 1) {
            for (j in 0 .. height - 2) {
                tu[i][j] = u[i][j] + dt * viscosity / density * laplacianu(i, j)
            }
        }
        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                tv[i][j] = v[i][j] + dt * viscosity / density * laplacianv(i, j)
            }
        }
        for (i in 1 .. width - 1) {
            for (j in 0 .. height - 2) {
                u[i][j] = tu[i][j]
            }
        }
        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                v[i][j] = tv[i][j]
            }
        }

    }

    fun isFluid(i : Int, j : Int) : Boolean {
        return i > 0 && i < width - 1
    }

    fun hasPressure(i : Int, j : Int) : Boolean {
        return i > 0 && i < width - 1 && j < height - 1
    }

    fun divergence(i : Int, j : Int) : Double {
        var res = 0.0
        res += u[i + 1][j] - u[i][j]
        res += v[i][j + 1] - v[i][j]
        return res
    }

    fun calcPressure() {
        val neighbours = arrayOf(Pair(-1, 0), Pair(1, 0), Pair(0, -1), Pair(0, 1))

        val scale = density / dt

        val B = BasicVector(fcount)

        for (i in 1 .. width - 2) {
            for (j in 0 .. height - 2) {
                val cid = id[i][j]
                B.set(cid, -divergence(i, j))

                for ((di, dj) in neighbours) {
                    val ni = i + di
                    val nj = j + dj

                    if (ni == 0) {
                        B.set(cid, B.get(cid) + (0.0 - u[ni][j])) // TODO
                    } else if (ni == width - 1) {
                        B.set(cid, B.get(cid) + (u[ni][j] - 0.0)) // TODO
                    } else if (nj == -1) {
//                        A.set(cid, cid, A.get(cid, cid) + 1.0)
                    } else if (nj == height - 1) {
                        B.set(cid, B.get(cid) + (v[i][nj] - sourceV))
                    } else {
                        val nid = id[ni][nj]
//                        A.set(cid, cid, A.get(cid, cid) + 1.0)
//                        A.set(cid, nid, -1.0);
                    }

                }
                B.set(cid, B.get(cid) * scale)
            }
        }

        var P = solvePressure(B)
//        println(B)
        for (i in 0..width-1) {
            for (j in 0..height-1) {
                if (hasPressure(i, j)) {
                    pressure[i][j] = P.get(id[i][j])
                }
            }
        }
    }

    fun applyPressure() {
        val scale = dt / density
        for (i in 0..width-1) {
            for (j in 0..height-1) {
                if (hasPressure(i, j)) {
                    u[i][j] -= scale * pressure[i][j]
                    u[i + 1][j] += scale * pressure[i][j]
                    v[i][j] -= scale * pressure[i][j]
                    v[i][j + 1] += scale * pressure[i][j]
                }
            }
        }
        for (j in 0..height-1) {
//            u[0][j] = 0.0
            u[1][j] = 0.0
            u[width - 1][j] = 0.0
        }
        for (i in 0..width-1) {
            v[i][height - 1] = sourceV
        }
    }

    fun moveMarkers() {
        for (m in markers) {
            val (vx, vy) = interpolateVelocity(m.x, m.y)
            m.x += vx * dt
            m.y += vy * dt
        }
    }
//
    fun extrapolate() {
        for (j in 0..height-1) {
            v[0][j] = - v[1][j]
            v[width - 1][j] = - v[width - 2][j]
        }
    }

    fun draw(g : Graphics) {
        fun dbg() {
            g.setColor(Color.BLACK)
            for (i in 0..width-1) {
                for (j in 0..height-1) {
                    val u = "%.2f".format(u[i][j])
                    val ux = i * cellSizeDbg
                    val uy = height * cellSizeDbg - (j + 0.5) * cellSizeDbg

                    val v = "%.2f".format(v[i][j])
                    val vx = (i + 0.5) * cellSizeDbg
                    val vy = height * cellSizeDbg - j * cellSizeDbg

                    val p = "%.2f".format(pressure[i][j])
                    val px = (i + 0.5) * cellSizeDbg
                    val py = height * cellSizeDbg - (j + 0.5) * cellSizeDbg

                    g.drawString(u, ux.toInt(), uy.toInt())
                    g.drawString(v, vx.toInt(), vy.toInt())
                    g.drawString(p, px.toInt(), py.toInt())
                }
            }
        }

        //        val g2d = g as Graphics2D
        //        val os = g2d.getStroke()
        //        g2d.setStroke(BasicStroke(2.0f))

        for (i in 0..width-1) {
            for (j in 0..height-1) {
                if (isFluid(i, j)) {
                    g.setColor(Color.BLUE)
                    g.fillRect(i * cellSizeDbg, height * cellSizeDbg - cellSizeDbg - j * cellSizeDbg, cellSizeDbg, cellSizeDbg)
                    g.setColor(Color.BLACK)
                    g.drawRect(i * cellSizeDbg, height * cellSizeDbg - cellSizeDbg - j * cellSizeDbg, cellSizeDbg, cellSizeDbg)
                }
            }
        }
        for (m in markers) {
            g.setColor(Color.GREEN)
            g.fillOval(((m.x + 0.5) * cellSizeDbg).toInt(), height * cellSizeDbg - ((m.y + 0.5) * cellSizeDbg).toInt(), 3, 3)
        }
//        dbg()
    }

    fun debug(fname : String, suffix : String) {
        val img = BufferedImage(width * cellSizeDbg, height * cellSizeDbg, BufferedImage.TYPE_INT_ARGB)
        val gr = img.createGraphics()!!
        gr.setBackground(Color.WHITE)
        gr.clearRect(0, 0, width * cellSizeDbg, height * cellSizeDbg)
        gr.setBackground(Color.BLACK)
        //        gr.drawString("fsdfsdf", 100, 100)
        draw(gr)
        //        gr.translate(0, height)
        //        gr.scale(1.0, -1.0)
        gr.dispose()
        ImageIO.write(img, "png", File("debug/%s%02d%s.png".format(fname, step, suffix)))
    }

    fun nextFrame() {
        println("===================================================")
        println("===============NEXT FRAME==========================")
        println("===================================================")
        var ct = 0.0
        while (ct < fdt - 0.000001) { // TODO epsilon
            dt = Math.min(fdt - ct, updatedt())
            advance()
            ct += dt
//            println("ct = %f".format(ct))
        }
        println("===================================================")
        println("===================================================")
        println("===================================================")
    }

    fun advance() {
        println("==================================")
        println("Step %d".format(step))

//        if (DEBUG)
//            debug("step", "_0m")


        update()
        println("Updated")
        if (DEBUG)
            debug("step", "_1u")

        println("dt = %f".format(dt))

        extrapolate()
        println("Extrapolated")
        if (DEBUG)
            debug("step", "_2e")

//        dt = updatedt()

        applyConvection()
        println("Applied convection")
        if (DEBUG)
            debug("step", "_3c")


        applyViscosity()
        println("Applied viscosity")
        if (DEBUG)
            debug("step", "_4v")

        calcPressure()
        if (DEBUG)
            debug("step", "_5c")
        println("Calculated pressure")


        applyPressure()
        println("Applied pressure")
        if (DEBUG)
            debug("step", "_6p")

        moveMarkers()
        println("Moved %d markers".format(markers.size))

        step++
        println("==================================")
    }


}