package simulator

interface Field<T> {
    fun add(a : T, b : T) : T
    fun sub(a : T, b : T) : T {
        return add(a, ainverse(b))
    }
    fun ainverse(a : T) : T
    fun aone() : T
    fun multiply(a : T, b : T) : T
    fun left_divide(a : T, b : T) : T {
        return multiply(minverse(a), b)
    }
    fun right_divide(a : T, b : T) : T {
        return multiply(a, minverse(b))
    }
    fun minverse(a : T) : T
    fun mone() : T
}