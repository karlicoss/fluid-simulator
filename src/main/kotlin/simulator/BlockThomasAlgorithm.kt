package simulator

import org.la4j.matrix.Matrix
import org.la4j.matrix.dense.Basic2DMatrix
import java.util.ArrayList


class RealField() : Field<Double> {
    override fun add(a: Double, b: Double): Double {
        return a + b
    }

    override fun ainverse(a: Double): Double {
        return -a
    }
    override fun aone(): Double {
        return 0.0
    }
    override fun multiply(a: Double, b: Double): Double {
        return a * b
    }
    override fun minverse(a: Double): Double {
        return 1.0 / a
    }
    override fun mone(): Double {
        return 1.0
    }
}

class MatrixField(val n : Int) : Field<Matrix> {
    override fun add(a: Matrix, b: Matrix): Matrix {
        return a.add(b)!!
    }
    override fun ainverse(a: Matrix): Matrix {
        return a.multiply(-1.0)!!
    }
    override fun aone(): Matrix {
        val res = Basic2DMatrix(n, n)
        return res
    }
    override fun multiply(a: Matrix, b: Matrix): Matrix {
        return a.multiply(b)!!
    }
    override fun minverse(a: Matrix): Matrix {
        val ta = a.copy()!!
        val ai = Basic2DMatrix(n, n)
        for (i in 0..n-1) {
            ai.set(i, i, 1.0)
        }
        for (i in 0..n-1) {
            var mj = i
            var m = Math.abs(ta.get(i, i))
            for (j in i + 1 .. n - 1) {
                if (Math.abs(ta.get(j, i)) >= m) {
                    mj = j
                    m = Math.abs(ta.get(j, i))
                }
            }
            ta.swapRows(i, mj)
            ai.swapRows(i, mj)
            val c = ta.get(i, i)
            ta.setRow(i, ta.getRow(i)!!.divide(c))
            ai.setRow(i, ai.getRow(i)!!.divide(c))
            for (j in i + 1 .. n - 1) {
                val c = ta.get(j, i)
                ta.setRow(j, ta.getRow(j)!!.subtract(ta.getRow(i)!!.multiply(c)))
                ai.setRow(j, ai.getRow(j)!!.subtract(ai.getRow(i)!!.multiply(c)))
            }
        }
        for (i in IntProgression.fromClosedRange(n - 1, 0, -1)) {
            for (j in IntProgression.fromClosedRange(i - 1, 0, -1)) {
                val c = ta.get(j, i) / ta.get(i, i)
                ta.setRow(j, ta.getRow(j)!!.subtract(ta.getRow(i)!!.multiply(c)))
                ai.setRow(j, ai.getRow(j)!!.subtract(ai.getRow(i)!!.multiply(c)))
            }
        }
//        return a.withInverter(LinearAlgebra.GAUSS_JORDAN)!!.inverse()!!
        return ai
    }
    override fun mone(): Matrix {
        val res = Basic2DMatrix(n, n)
        for (i in 0..n-1) {
            res.set(i, i, 1.0)
        }
        return res
    }

}


class BlockThomasAlgorithm<T> (
        val n : Int,
        val f : Field<T>,
        val a : ArrayList<T>,
        val b : ArrayList<T>,
        val c : ArrayList<T>
) {
    val cc = ArrayList<T>(c)
    val ee = ArrayList<T>(c)
    init {
        if (a.size != n) {
            throw IllegalArgumentException()
        }
        if (b.size != n) {
            throw IllegalArgumentException()
        }
        if (c.size != n) {
            throw IllegalArgumentException()
        }
        for (i in 0 .. n - 2) {
            if (i == 0) {
                cc[i] = f.left_divide(b[i], c[i])
            } else {
                cc[i] = f.left_divide(f.sub(b[i], f.multiply(a[i], cc[i - 1])), c[i])
            }
        }

        for (i in 0 .. n - 1) {
            if (i == 0) {
                ee[i] = b[i]
            } else {
                ee[i] = f.sub(b[i], f.multiply(a[i], cc[i - 1]))
            }
        }
    }

    fun compute(d: ArrayList<T>) : ArrayList<T> {
        if (d.size != n) {
            throw IllegalArgumentException()
        }
        val dd = ArrayList<T>(c)
        for (i in 0 .. n - 1) {
            if (i == 0) {
                dd[i] = f.left_divide(ee[i], d[i])
            } else {
                dd[i] = f.left_divide(ee[i], f.sub(d[i], f.multiply(a[i], dd[i - 1])))
            }
        }
        val x = ArrayList<T>(d)
        for (i in IntProgression.fromClosedRange(n - 1, 0, -1)) {
            if (i == n - 1) {
                x[i] = dd[i]
            } else {
                x[i] = f.sub(dd[i], f.multiply(cc[i], x[i + 1]))
            }
        }
        return x
    }
}