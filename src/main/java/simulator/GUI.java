package simulator;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GUI {

    private JPanel MainGUI;
    private JLabel labelReynolds;
    private JTextField inputViscosity;
    private JTextField inputWidth;
    private JTextField inputDensity;
    private JTextField inputVelocity;
    private JTextField inputHeight;
    private JTextField inputCellSize;
    private JTextField inputTimestep;
    private JButton startButton;
    private JButton stopButton;
    private GLJPanel canvas;
    private JTextField inputMarkerfreq;
    private Environment environment;
    private int width;
    private int height;
    private int cellSize;

    private GUI() {
        $$$setupUI$$$();
        startButton.addActionListener(
                (ActionEvent e) -> {
                    double viscosity = Double.parseDouble(inputViscosity.getText());
                    double density = Double.parseDouble(inputDensity.getText());
                    double velocity = Double.parseDouble(inputVelocity.getText());
                    width = Integer.parseInt(inputWidth.getText());
                    height = Integer.parseInt(inputHeight.getText());
                    cellSize = Integer.parseInt(inputCellSize.getText());
                    double fdt = Double.parseDouble(inputTimestep.getText());
                    int mfreq = Integer.parseInt(inputMarkerfreq.getText());

                    double re = density * Math.abs(velocity) * (width / cellSize) / viscosity;
                    labelReynolds.setText(String.format("%.3f", re));

                    environment = new Environment(width, height, cellSize, fdt, viscosity, density, mfreq, velocity);

                    Dimension dimension = new Dimension(width + 2 * cellSize, height + cellSize);
                    canvas.setPreferredSize(dimension);
                    MainGUI.revalidate();

                    // TODO that's weids, there's gotta be a proper way of expanding the main window
                    SwingUtilities.getWindowAncestor(MainGUI).pack();
                }
        );
        stopButton.addActionListener(
                (ActionEvent e) -> {
                    environment = null;
                }
        );
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(
                () -> {
                    JFrame frame = new JFrame();
                    frame.setContentPane(new GUI().MainGUI);
                    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);
                }
        );
    }

    private void createUIComponents() {
        GLProfile glp = GLProfile.getDefault();
        GLCapabilities glcaps = new GLCapabilities(glp);
        canvas = new GLJPanel(glcaps);
        final FPSAnimator animator = new FPSAnimator(canvas, 60);
        canvas.setPreferredSize(new Dimension(400, 400));
        canvas.addGLEventListener(new GLEventListener() {

            @Override
            public void init(GLAutoDrawable glAutoDrawable) {
                GL2 gl = glAutoDrawable.getGL().getGL2();
                gl.glColor3f(0.0f, 0.0f, 1.0f);
                gl.glPointSize(3.0f);
            }

            @Override
            public void dispose(GLAutoDrawable glAutoDrawable) {

            }

            @Override
            public void display(GLAutoDrawable glAutoDrawable) {
                if (environment == null) {
                    return;
                }
                environment.nextFrame();

                final GL2 gl = glAutoDrawable.getGL().getGL2();
                gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                gl.glClear(GL.GL_DEPTH_BUFFER_BIT);


                gl.glColor3d(0.73, 0.82, 0.96);
                gl.glRectd(cellSize, 0, cellSize + width, height + cellSize);

                gl.glColor3d(1.0, 0.0, 1.0);
                gl.glBegin(GL.GL_POINTS);
                for (Marker m : environment.getMarkers()) {
                    gl.glColor3d(m.getColorr(), m.getColorg(), m.getColorb());
                    gl.glVertex2d((m.getX() + 0.5) * cellSize, (m.getY() + 0.5) * cellSize);
                }
                gl.glEnd();
            }

            @Override
            public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                GL2 gl = glAutoDrawable.getGL().getGL2();
                gl.glViewport(0, 0, width, height);
                gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
                gl.glLoadIdentity();
                gl.glOrtho(0.0, width, 0.0, height, 1.0, -1.0);
            }
        });
        animator.start();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        MainGUI = new JPanel();
        MainGUI.setLayout(new GridBagLayout());
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        MainGUI.add(panel1, gbc);
        labelReynolds = new JLabel();
        labelReynolds.setText("Label");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(labelReynolds, gbc);
        final JLabel label1 = new JLabel();
        label1.setText("Reynolds number");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label1, gbc);
        inputViscosity = new JTextField();
        inputViscosity.setText("1.0");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputViscosity, gbc);
        inputWidth = new JTextField();
        inputWidth.setText("400");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputWidth, gbc);
        inputDensity = new JTextField();
        inputDensity.setText("1.0");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputDensity, gbc);
        inputVelocity = new JTextField();
        inputVelocity.setText("-1.0");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputVelocity, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("Viscosity");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label2, gbc);
        final JLabel label3 = new JLabel();
        label3.setText("Velocity");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label3, gbc);
        final JLabel label4 = new JLabel();
        label4.setText("Density");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label4, gbc);
        final JLabel label5 = new JLabel();
        label5.setText("Width");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label5, gbc);
        final JLabel label6 = new JLabel();
        label6.setText("Height");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label6, gbc);
        final JLabel label7 = new JLabel();
        label7.setText("Cell size");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label7, gbc);
        inputHeight = new JTextField();
        inputHeight.setText("600");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputHeight, gbc);
        inputCellSize = new JTextField();
        inputCellSize.setText("50");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputCellSize, gbc);
        inputTimestep = new JTextField();
        inputTimestep.setText("0.01");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputTimestep, gbc);
        final JLabel label8 = new JLabel();
        label8.setText("Time step");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label8, gbc);
        inputMarkerfreq = new JTextField();
        inputMarkerfreq.setText("200");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 8;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 10);
        panel1.add(inputMarkerfreq, gbc);
        final JLabel label9 = new JLabel();
        label9.setText("Markers frequency");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label9, gbc);
        startButton = new JButton();
        startButton.setText("Start");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(startButton, gbc);
        stopButton = new JButton();
        stopButton.setText("Stop");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(stopButton, gbc);
        final JToolBar.Separator toolBar$Separator1 = new JToolBar.Separator();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 10;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(toolBar$Separator1, gbc);
        final JLabel label10 = new JLabel();
        label10.setText("            ");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label10, gbc);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        MainGUI.add(canvas, gbc);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainGUI;
    }
}
