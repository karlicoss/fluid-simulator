package simulator

import org.junit.Assert
import org.junit.Test
import org.la4j.matrix.dense.Basic2DMatrix

public class TestReal {

    @Test fun testInverse() {
        val n = 3
        val m = Basic2DMatrix(n, n)
        m.set(0, 0, 2.0)
        m.set(1, 1, 3.0)
        m.set(2, 2, 5.0)
        val f = MatrixField(n)
        println(f.minverse(m))
    }

    @Test fun testDouble() {
        val n = 3
        val a = arrayListOf( 0.0, -1.0, -1.0)
        val b = arrayListOf( 2.0,  3.0,  2.0)
        val c = arrayListOf(-1.0, -1.0,  0.0)
        val d = arrayListOf(1.0, 1.0, 1.0)
        val m = BlockThomasAlgorithm(n, RealField(), a, b, c)
        val x = m.compute(d)
        Assert.assertArrayEquals(arrayOf(1.0, 1.0, 1.0), x.toArray(arrayOf(0.0)))
    }

    @Test fun testMatrix0() {
        val n = 2
        val k = 2
        val Z = Basic2DMatrix(k, k)
        val D = Basic2DMatrix(k, k)
        D.set(0, 0, 3.0)
        D.set(1, 1, 2.0)
        D.set(0, 1, -1.0)
        D.set(1, 0, -1.0)
//        println(D)

        val I = Basic2DMatrix(k, k)
        I.set(0, 0, -1.0)
        I.set(1, 1, -1.0)
//        println(I)
        val R = Basic2DMatrix(k, k)
        R.set(1, 1, 200.0)
        val a = arrayListOf(Z.copy()!!, I.copy()!!)
        val b = arrayListOf(D.copy()!!, D.copy()!!)
        val c = arrayListOf(I.copy()!!, Z.copy()!!)
        val d = arrayListOf(R.copy()!!, R.copy()!!)
        val m = BlockThomasAlgorithm(n, MatrixField(k), a, b, c)
        val x = m.compute(d)
        println(x)
        val xx = doubleArrayOf(
                x[0].getRow(0)!!.sum(),
                x[0].getRow(1)!!.sum(),
                x[1].getRow(0)!!.sum(),
                x[1].getRow(1)!!.sum()
        )
        Assert.assertArrayEquals(doubleArrayOf(200.0, 400.0, 200.0, 400.0), xx, 0.01)


    }

    @Test fun testMatrix() {
        val n = 2
        val k = 4
        val Z = Basic2DMatrix(k, k)
        val D = Basic2DMatrix(k, k)
        D.set(0, 0, 3.0)
        D.set(1, 1, 3.0)
        D.set(2, 2, 3.0)
        D.set(3, 3, 2.0)
        D.set(0, 1, -1.0)
        D.set(1, 2, -1.0)
        D.set(2, 3, -1.0)
        D.set(1, 0, -1.0)
        D.set(2, 1, -1.0)
        D.set(3, 2, -1.0)
//        println(D)

        val I = Basic2DMatrix(k, k)
        I.set(0, 0, -1.0)
        I.set(1, 1, -1.0)
        I.set(2, 2, -1.0)
        I.set(3, 3, -1.0)
//        println(I)
        val R = Basic2DMatrix(k, k)
        R.set(3, 3, 200.0)
        val a = arrayListOf(Z.copy()!!, I.copy()!!)
        val b = arrayListOf(D.copy()!!, D.copy()!!)
        val c = arrayListOf(I.copy()!!, Z.copy()!!)
        val d = arrayListOf(R.copy()!!, R.copy()!!)
        val m = BlockThomasAlgorithm(n, MatrixField(k), a, b, c)
        val x = m.compute(d)
        val xx = doubleArrayOf(
                x[0].getRow(0)!!.sum(),
                x[0].getRow(1)!!.sum(),
                x[0].getRow(2)!!.sum(),
                x[0].getRow(3)!!.sum(),
                x[1].getRow(0)!!.sum(),
                x[1].getRow(1)!!.sum(),
                x[1].getRow(2)!!.sum(),
                x[1].getRow(3)!!.sum()
        )
        Assert.assertArrayEquals(
                doubleArrayOf(200.0, 400.0, 600.0, 800.0, 200.0, 400.0, 600.0, 800.0),
                xx,
                0.01
        )
    }
}
