This project is a Navier-Stokes fluid solver for a pipe segment. Implemented on Java and Kotlin.

Demo videos:

* [Steady flow](https://www.youtube.com/watch?v=aGQ2jYPD304)
* [Turbulent flow](https://youtu.be/ZDNDSQm-ov0)

**Disclaimer**: the code is quite old, I wish I could find time to rewrite it to LAPACK for faster
simulation. Also, the UI could be improved as well :)

# Running

You don't really need anything except Java 8, Gradle takes care of all the libraries.

    ./gradlew run

# Interface
* To speed the flow up, reduce the velocity (it is negative since the flow is going downwards)
* To observe more markers, decrease 'markers frequency'
* Reducing 'Cell size' and 'Time step' results in more precise (but more CPU consuming) computation
* If you change some parameters, just click 'Start' again to apply them and rerun the simulation.
* Once the flow get turbulent enough, the simulation stops
(since the velocities are too high to do meaningful computations).


##  Reynolds number
Depending on the setup, you will get different [Reynolds number](https://en.wikipedia.org/wiki/Reynolds_number).

Reynolds number less than 10 means than the flow will be laminar and steady.

Big Reynolds number (e.g. ~ 1000) will likely result in turbulent flow.